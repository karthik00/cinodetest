/*
 * Copyright Syncth (c) 2012.
 *
 * This code is intellectual property of Syncth. Written and maintained my AlphaDevs.
 * WARN::Possession of this code by any third party will be considered as theft of intellectual property.
 */

var express = require('express')
    , controllers = require('./controllers')
    , conf =   require('./conf')
    , http = require('http')
    , nowjs = require("now");

var app = express();

const helper = require('express-helpers')(app);

conf.appconfig.configure(app);
conf.routing_table.configureRoutes(app,controllers)

var server=http.createServer(app);
server.listen(app.get('port'), function(){
  console.log("Express server listening on port " + app.get('port'));
});

//global variables sessions and rpc config
GLOBAL.everyone = nowjs.initialize(server);
everyone.now.getServerInfo = controllers.getServerInfo;
everyone.now.sendVerificationSMS = controllers.sendVerificationSMS;

GLOBAL._ = require("underscore");