/*
 * Copyright Syncth (c) 2012.
 *
 * This code is intellectual property of Syncth. Written and maintained my AlphaDevs.
 * WARN::Possession of this code by any third party will be considered as theft of intellectual property.
 */

module.exports.configure=function (app){
    var expressLayouts = require('express-ejs-layouts')
        , path = require('path')
        , express = require('express')
    app.configure(function () {
        app.set('port', process.env.PORT || 3000);
        app.set('layout', 'layout')
        app.set('views', __dirname + '/views');
        app.set('view engine', 'ejs');
        app.use(expressLayouts)
        app.use(express.favicon());
        app.use(express.logger('dev'));
        app.use(express.bodyParser());
        app.use(express.methodOverride());
        app.use(app.router);
        app.use(express.static(path.join(__dirname, 'public')));
    });

    app.configure('development', function(){
        app.use(express.errorHandler());
    });
}