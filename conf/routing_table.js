/*
 * Copyright Syncth (c) 2012.
 *
 * This code is intellectual property of Syncth. Written and maintained my AlphaDevs.
 * WARN::Possession of this code by any third party will be considered as theft of intellectual property.
 */

exports.configureRoutes=function (app, routes) {
    app.get('/', routes.index);
}