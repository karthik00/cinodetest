/*
 * Copyright Syncth (c) 2012.
 *
 * This code is intellectual property of Syncth. Written and maintained my AlphaDevs.
 * WARN::Possession of this code by any third party will be considered as theft of intellectual property.
 */

var crypto = require('crypto');

exports.index = function(req, res){
	res.render('index', { title: 'Express', scripts:['index', 'md5']});
};

exports.sendVerificationSMS=function(){
	var rand=Math.floor((Math.random()*10000)+1);
	console.log(rand)
	var md5sum = crypto.createHash('md5');
	md5sum.update(rand+"")
	this.now.smsVerification=md5sum.digest('hex');
	this.now.showSMSVarificationInput();
}