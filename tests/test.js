/*
 * Copyright Syncth (c) 2012.
 *
 * This code is intellectual property of Syncth. Written and maintained my AlphaDevs.
 * WARN::Possession of this code by any third party will be considered as theft of intellectual property.
 */

exports.testSomething = function(test){
    test.expect(1);
    test.ok(true, "this assertion should pass");
    test.done();
};

exports.testSomethingElse = function(test){
    test.ok(false, "this assertion should fail");
    test.done();
};